#define TOT_POS1 3100
#define TOT_POS2 1000
#define ALIEN_SERVO_CHANNEL 2
#define SERVO_PERIOD 2500

//Initialize PWM code
PWM_TIVA_Init(3);
PWM_TIVA_SetPeriod(SERVO_PERIOD, 1);  //group 1 encompasses TOT and Alien servo
PWM_TIVA_SetPulseWidth(SERVO_DOWN ,ALIEN_SERVO_CHANNEL);

void TotServoToggle(bool pos);

//Sets TOT motor to either 0 or 90 degree position depending on the state of input bool 'pos'. Exact orientation can be determined by mounting hardware later
void TotServoToggle(bool pos)
{
    if (pos == 1)
    {
      PWM_TIVA_SetPulseWidth(TOT_POS1 ,ALIEN_SERVO_CHANNEL);    
    }
    if (pos == 0)
    {
      PWM_TIVA_SetPulseWidth(TOT_POS2 ,ALIEN_SERVO_CHANNEL);    
    }
}